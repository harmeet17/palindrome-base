package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * harmeet kaur
 * 991539912
 */
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Unvalid test result", Palindrome.isPalindrome("racecar"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Unvalid test result", Palindrome.isPalindrome("gurtej"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Unvalid test result", Palindrome.isPalindrome("race car"));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Unvalid test result", Palindrome.isPalindrome("race a car"));
	}
}
